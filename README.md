# 💙 Blue Couch AI | client

### 📖 Get Started Immediately

You **don’t** need to install or configure tools like webpack or Babel.
 They are preconfigured that you can focus on the code.



## 📌 Requirements

- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

- Vagrant 2.2.10 or higher

- Create enviroment file `cp .env.example .env`

  

- #### Install Vagrant

  Installing Vagrant is extremely easy. Head over to the [Vagrant downloads page](https://www.vagrantup.com/downloads) and get the appropriate installer or package for your platform. Install the package using standard procedures for your operating system.

  The installer will automatically add `vagrant` to your system path so that it is available in terminals. If it is not found, please try logging out and logging back in to your system (this is particularly necessary sometimes for Windows).

  

- #### Usage

  In your terminal put in `vagrant up` and run it. it might take a few minutes, then run `vagrant ssh` in terminal.

  in the guest os run `cd /vagrant && yarn start`. 

  React development server will run on port 3000 [here](http://localhost:3000/) !
  
  

**We're use Node version 12.18.4 LTS on vagrant development machine**

