# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 2.2.10"

unless Vagrant.has_plugin?("vagrant-env")
  system("vagrant plugin install vagrant-env")
  puts "Dependencies installed, please try the command again."
  exit
end

Vagrant.configure("2") do |config|
  config.env.enable # Enable vagrant-env(.env)
  config.vm.box = ENV["V_BASE_IMAGE_NAME"] || "ubuntu/trusty64"
  config.vm.network "private_network", type: "dhcp"
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.synced_folder ".", "/vagrant", type: "nfs"
  config.vagrant.plugins = ["vagrant-env", "vagrant-reload"]
  config.vm.provision :shell, inline: "apt-get update"

  config.vm.provider "virtualbox" do |v|
    v.name = "blue_couch_ai_front_virtualbox"
    v.check_guest_additions = false
    v.memory = ENV["V_MEMORY_ASSIGNED"] || 4096
    v.cpus = ENV["V_CPU_CORES"] || 2
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "70"]
   end

  config.vm.provision 'preemptively give others write access to /etc/ansible/roles', type: :shell, inline: "mkdir /etc/ansible/roles -p && chmod o+w /etc/ansible/roles"

  config.vm.provision "ansible_local" do |ansible|
    ansible.extra_vars         = {
          NODE_VERSION: ENV["ANS_NODE_VERSION"] || "12.18.4",
    }
    ansible.playbook          = "./provisioning/playbook.yml"
    ansible.become            = true
    ansible.verbose           = true
    ansible.install           = true
    ansible.config_file       = "./provisioning/ansible.cfg"

  end

end
